import eslintPluginCommentLength from "eslint-plugin-comment-length";

const maxLength = 120;

export default [
  eslintPluginCommentLength.configs["flat/recommended"],
  {
    rules: {
      "comment-length/limit-single-line-comments": [
        "error",
        {
          maxLength,
        },
      ],
      "comment-length/limit-multi-line-comments": [
        "error",
        {
          maxLength,
        },
      ],
    },
  },
];
