# try-eslint-plugin-comment-length

Try [eslint-plugin-comment-length](https://github.com/lasselupe33/eslint-plugin-comment-length) and [Rewrap](https://stkb.github.io/Rewrap/) to format CSS and JavaScript comments.

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
touch index.js
```

```bash
npm run lint
```

```bash
npm run format
```
