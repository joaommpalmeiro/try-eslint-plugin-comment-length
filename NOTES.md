# Notes

- [Support ESLint v9](https://github.com/lasselupe33/eslint-plugin-comment-length/issues/10) issue
- https://github.com/lasselupe33/eslint-plugin-comment-length/blob/v1.7.3/rules/package.json
- Rewrap:
  - https://marketplace.visualstudio.com/items?itemName=stkb.rewrap
  - https://github.com/stkb/Rewrap/
  - https://stkb.github.io/Rewrap/
  - https://stkb.github.io/Rewrap/configuration/#wrapping-column:
    - "If neither this nor any rulers are set, then VS Code's `editor.wordWrapColumn` setting is used. This has a default of 80."
  - https://stkb.github.io/Rewrap/specs/content-types/javadoc/
  - https://stkb.github.io/Rewrap/specs/content-types/python/

## Commands

```bash
npm install eslint@8 eslint-plugin-comment-length
```
